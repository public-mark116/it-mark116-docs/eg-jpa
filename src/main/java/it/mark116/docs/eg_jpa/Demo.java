package it.mark116.docs.eg_jpa;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import it.mark116.docs.eg_jpa.entity.Address;
import it.mark116.docs.eg_jpa.entity.Customer;
import it.mark116.docs.eg_jpa.entity.Order;
import it.mark116.docs.eg_jpa.entity.Product;
import it.mark116.docs.eg_jpa.repository.CustomerRepository;
import it.mark116.docs.eg_jpa.repository.OrderRepository;
import it.mark116.docs.eg_jpa.repository.ProductRepository;

public class Demo {
	public static void main(String[] args) {
		// Creazione degli entityManager
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("egJpa");
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		// Creazione della transazione
		entityManager.getTransaction().begin();

		// Creazione dei repository
		CustomerRepository customerRepository = new CustomerRepository(entityManager);
		OrderRepository orderRepository = new OrderRepository(entityManager);
		ProductRepository productRepository = new ProductRepository(entityManager);

		// Salvataggio di un nuovo cliente (con indirizzo)
		Customer customer1 = new Customer("Mario", "Rossi");
		customer1.setAddress(new Address("Piazza Duomo", 1, "Milano", "Italia"));
		customerRepository.save(customer1);
		System.out.println("Cliente salvato!");

		// Salvataggio di un nuovo cliente (senza indirizzo)
		Customer customer2 = new Customer("Chiara", "Bianchi");
		customerRepository.save(customer2);
		System.out.println("\nCliente salvato!");

		// Selezione di tutti i clienti
		List<Customer> customers = customerRepository.findAll();
		System.out.println("\nEstrazione elenco clienti:");
		customers.forEach(x -> System.out.println(x.toString()));

		// Selezione del cliente per ID
		Optional<Customer> customerById = customerRepository.findById(1L);
		System.out.println("\nEstrazione cliente per ID:");
		customerById.ifPresent(x -> System.out.println(x.toString()));

		// Selezione del cliente per nome
		Optional<Customer> customerByName = customerRepository.findByFirstName("Chiara");
		System.out.println("\nEstrazione cliente per nome:");
		customerByName.ifPresent(x -> System.out.println(x.toString()));
		
		// Selezione del cliente per cognome
		Optional<Customer> customerBySurname = customerRepository.findByLastName("Bianchi");
		System.out.println("\nEstrazione cliente per cognome:");
		customerBySurname.ifPresent(x -> System.out.println(x.toString()));

		// Salvataggio di un nuovo ordine
		customerById.ifPresent(c -> {
			customerById.get().setOrder(new Order(5, c));
			customerRepository.save(customerById.get());
			System.out.println("\nOrdini salvati!");
		});

		// Salvataggio di un nuovo ordine
		customerByName.ifPresent(c -> {
			customerByName.get().setOrder(new Order(50, c));
			customerByName.get().setOrder(new Order(20, c));
			customerRepository.save(customerByName.get());
			System.out.println("\nOrdini salvati!");
		});

		// Selezione di tutti gli ordini
		List<Order> orders = orderRepository.findAll();
		System.out.println("\nEstrazione elenco ordini:");
		orders.forEach(x -> System.out.println(x.toString()));

		// Selezione dell'ordine per ID
		Optional<Order> orderById = orderRepository.findById(1L);
		System.out.println("\nEstrazione ordine per ID:");
		orderById.ifPresent(x -> System.out.println(x.toString()));

		// Selezione degli ordini per cliente
		customerByName.ifPresent(c -> {
			List<Order> ordersByCustomer = c.getOrders();
			System.out.println("\nEstrazione ordine per cliente:");
			ordersByCustomer.forEach(x -> System.out.println(x.toString()));
		});

		// Creazione di un nuovo prodotto
		Product product1 = new Product("Prodotto1", 10);
		productRepository.save(product1);
		System.out.println("\nProdotto salvato!");

		Product product2 = new Product("Prodotto2", 60);
		productRepository.save(product2);
		System.out.println("\nProdotto salvato!");

		// Salvataggio di un nuovo ordine con prodotti
		customerByName.ifPresent(c -> {
			Order order3 = new Order();
			order3.setCustomer(c);
			order3.setProduct(product1);
			order3.setProduct(product2);
			orderRepository.save(order3);
			System.out.println("\nOrdine salvato!");
		});

		// (Ri-)Selezione di tutti gli ordini
		orders = orderRepository.findAll();
		System.out.println("\nEstrazione elenco ordini:");
		orders.forEach(System.out::println);

		// Estrazione di tutti i prodotti di un dato ordine
		Optional<Order> ordersById = orderRepository.findById(4);
		System.out.println("\nEstrazione elenco prodotti ultimo ordine:");
		if(ordersById.isPresent()) {
			ordersById.get().getProducts().forEach(System.out::println);
		}

		// Commit delle operazioni
		entityManager.getTransaction().commit();

		// Chiusura della entityManager
		entityManager.close();
		entityManagerFactory.close();
	}
}
