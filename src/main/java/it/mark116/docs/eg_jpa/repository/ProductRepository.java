package it.mark116.docs.eg_jpa.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import it.mark116.docs.eg_jpa.entity.Product;

public class ProductRepository {

	private EntityManager entityManager;

	public ProductRepository(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Optional<Product> findById(long id) {
		Product product = entityManager.find(Product.class, id);
		return product != null ? Optional.of(product) : Optional.empty();
	}

	public List<Product> findAll() {
		return entityManager.createQuery("from Product").getResultList();
	}

	public Product save(Product product) {
		if(product.getId() == null) {
			entityManager.persist(product);
		}
		else {
			product = entityManager.merge(product);
		}

		return product;
	}

	public void delete(Product product) {
		if(entityManager.contains(product)) {
			entityManager.remove(product);
		}
		else {
			entityManager.merge(product);
		}
	}
}
