package it.mark116.docs.eg_jpa.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import it.mark116.docs.eg_jpa.entity.Order;

public class OrderRepository {

	private EntityManager entityManager;

	public OrderRepository(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Optional<Order> findById(long id) {
		Order order = entityManager.find(Order.class, id);
		return order != null ? Optional.of(order) : Optional.empty();
	}

	public List<Order> findAll() {
		return entityManager.createQuery("from Order").getResultList();
	}

	public Order save(Order order) {
		if(order.getId() == null) {
			entityManager.persist(order);
		}
		else {
			order = entityManager.merge(order);
		}

		return order;
	}

	public void delete(Order order) {
		if(entityManager.contains(order)) {
			entityManager.remove(order);
		}
		else {
			entityManager.merge(order);
		}
	}
}
