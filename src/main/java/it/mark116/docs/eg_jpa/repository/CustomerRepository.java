package it.mark116.docs.eg_jpa.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import it.mark116.docs.eg_jpa.entity.Customer;

public class CustomerRepository {

	private EntityManager entityManager;

	public CustomerRepository(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Optional<Customer> findById(long id) {
		Customer customer = entityManager.find(Customer.class, id);
		return customer != null ? Optional.of(customer) : Optional.empty();
	}

	public List<Customer> findAll() {
		return entityManager.createQuery("from Customer").getResultList();
	}

	public Optional<Customer> findByFirstName(String firstName) {
		Customer customer = entityManager.createQuery("SELECT c FROM Customer c WHERE c.firstName = :firstName", Customer.class).setParameter("firstName", firstName).getSingleResult();
		return Optional.ofNullable(customer);
	}

	public Optional<Customer> findByLastName(String lastName) {
		Customer customer = entityManager.createNamedQuery("Customer.findByLastName", Customer.class).setParameter("lastName", lastName).getSingleResult();
		return Optional.ofNullable(customer);
	}

	public Customer save(Customer customer) {
		if(customer.getId() == null) {
			entityManager.persist(customer);
		}
		else {
			customer = entityManager.merge(customer);
		}

		return customer;
	}

	public void delete(Customer customer) {
		if(entityManager.contains(customer)) {
			entityManager.remove(customer);
		}
		else {
			entityManager.merge(customer);
		}
	}
}
