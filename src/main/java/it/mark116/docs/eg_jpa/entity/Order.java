package it.mark116.docs.eg_jpa.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "customer_order")
public class Order {
	
	@Id
	@Column(name = "id_order")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private float amount = 0;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_customer")
	private Customer customer;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "order_product", joinColumns = { @JoinColumn(name = "id_order") }, inverseJoinColumns = { @JoinColumn(name = "id_product") })
	private List<Product> products = new ArrayList<>();

	public Order() {
	}

	public Order(float amount, Customer customer) {
		this.amount = amount;
		this.customer = customer;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public float getAmount() {
		return this.amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<Product> getProducts() {
		return this.products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
		this.amount = products.stream().map(Product::getAmount).reduce(0f, Float::sum);
	}

	public void setProduct(Product product) {
		this.products.add(product);
		this.amount += product.getAmount();
	}

	@Override
	public String toString() {
		return "(" + this.id + ") Cliente: " + this.getCustomer().getLastName() + ", Prezzo: " + this.amount + ", Totale articoli: " + this.products.size();
	}
}
