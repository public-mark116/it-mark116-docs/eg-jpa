package it.mark116.docs.eg_jpa.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Address {

	@Column(length = 20, nullable = true)
	private String streetName;

	@Column(nullable = true)
	private int streetNumber;

	@Column(nullable = true)
	private String city;

	@Column(nullable = true)
	private String state;

	public Address() {
	}

	public Address(String streetName, int streetNumber, String city, String state) {
		this.streetName = streetName;
		this.streetNumber = streetNumber;
		this.city = city;
		this.state = state;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public int getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(int streetNumber) {
		this.streetNumber = streetNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return this.streetName + " " + this.streetNumber + ", " + this.city + " - " + this.state;
	}

}
